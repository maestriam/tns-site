app.factory('Company',['$http', 'URL_BASE', 'TYPE_BASE', 'LocalStorage','User',
			  function($http, URL_BASE, TYPE_BASE, LocalStorage, User) {

	/**
	 * Salva as categorias das empresas no localstorage do usuário
	 * 
	 * @param  Object categories
	 * @return void
	 */
	var setLocalCategories = (categories) =>
	{
		LocalStorage.setObject('categories', categories);
	}

	/**
	 * Retornar os dados da categorias salvo no localstorage
	 * 
	 * @return Object
	 */
	var getLocalCategories = () =>
	{
		return LocalStorage.getObject('categories');
	}

	/**
	 * Retorna todas as empresas cadastradas no back-end
	 * 
	 * @param  function success 
	 * @param  function error   
	 * @return void
	 */
	var all = (success, error) =>
	{
        $http({
            url: URL_BASE + '/companies',
            dataType: 'json',
            method: 'GET',
            timeout: 15000,
        })
        .then(success, error);
	}

	/**
	 * Pesquisa empresas por 
	 * 
	 * @param  string   query   
	 * @param  function success 
	 * @param  function error   
	 * @return void
	 */
	var search = (query, categoryId, success, error) =>
	{
		var input = {
			term: query,
			category_id: categoryId
		}

		var callback = (response) => {
			success(response.data.companies);
		}

        $http({
            url: URL_BASE + '/search',
            dataType: 'json',
            method: 'POST',
            data: input,
            timeout: 15000,
        })
        .then(callback, error);		
	}

	/**
	 * Retorna uma categoria por ID	
	 * 
	 * @param  integer id 
	 * @return Object
	 */
	var categoryById = (id) => 
	{
		let categories = getLocalCategories();

		let cat = categories.filter((elem) => {            
            if (elem.id === id) {
                return elem;
            }
        })
        return cat[0].name;
	}	

	/**
	 * Retorna as categroias das empresas
	 * 
	 * @param  function success 
	 * @param  function error   
	 * @return void
	 */
	var categories = (success, error) => 
	{
		var categories = getLocalCategories();

		var callback = function(response)
		{
			var categories = response.data; 
			setLocalCategories(categories);
			success(categories);
		}

		$http({
            url: URL_BASE + '/categories',
            dataType: 'json',
            method: 'GET',
            timeout: 15000,
        })
        .then(callback, error);
	}

	/**
	 * Retorna as perguntas para abertura de chamado
	 * 
	 * @param  integer id       
	 * @param  function success 
	 * @param  function error   
	 * @return void
	 */
	var questions = (id, success, error) =>
	{
		var token = User.token();

		var callback = function(response)
		{
			console.log(response);
			success(response.data);
		}

		$http({
            url: URL_BASE + '/questions/'+ id + '?token=' + token,
            dataType: 'json',
            method: 'GET',
            timeout: 15000,
        })
        .then(callback, error);
	}


	/**
	 * Retorna as perguntas para abertura de chamado
	 * 
	 * @param  integer id       
	 * @param  function success 
	 * @param  function error   
	 * @return void
	 */
	var options = (id, success, error) =>
	{
		var token = User.token();

		var callback = function(response)
		{
			success(response.data);
		}

		var params = '/questions/'+ id + '/options?token=' + token;

		$http({
            url: URL_BASE + params,
            dataType: 'json',
            method: 'GET',
            timeout: 15000,
        })
        .then(callback, error);
	}


	return {
		all: all,
		search: search,
		options: options,
		questions: questions,
		categories: categories,
		categoryById: categoryById	
	}

}]);