app.controller('OptionsController', 
function ($scope, $stateParams, $location, Chat, Tap, Company, User, UserDialog) {

	var vm = $scope;

	vm.options = null;
	vm.id 	   = $stateParams.questionId;

	// Retorna todas as opções de uma abertura de chamdo
	Company.options(vm.id,
	(response) => {
		vm.companyId = response[0].customer_id;
		vm.options   = response[0].options;

		console.log(response);
	},
	(reason)=>{

	})

	// Se o problema foi resolvido
	vm.goHome = () => {
		$location.path('/');
	}

	// Se o problema NÃO for resolvido..
	vm.goChat = () => {


		if(User.isLogged() == false) {
			UserDialog.login();
			return false;
		}

		Chat.start(vm.companyId,
		(response) => {
			$location.path('/taps');
		},
		(reason)=> {
			console.log(reason);
		});
	}

});