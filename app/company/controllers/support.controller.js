app.controller('SupportController', function ($scope, $rootScope, $stateParams, $location, $mdDialog, Tap, Company, User) {
	
	var vm = $scope;

	vm.questions = [];
	vm.id = $stateParams.companyId;

	Company.questions(vm.id,
	(response) => {
		vm.questions = response;
	},
	(reason)=> {
		console.log(reason);
	});

});