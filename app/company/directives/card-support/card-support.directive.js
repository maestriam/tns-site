app.directive('cardSupport', ['$location', function ($location) {

	var basePath = 'app/company/directives/card-support';

    return {
        templateUrl: basePath + '/card-support.html',
        scope: {
            title: '@',
            id: '@'
        },
        link: function(scope) {

            scope.theme = 'default';

            scope.goOptions = () => {
                $location.path('/options/'+scope.id);
            }

        }
    };
}]);