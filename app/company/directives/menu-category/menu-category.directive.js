app.directive('menuCategory', function () {

	var basePath = 'app/company/directives/menu-category';

    return {
        templateUrl: basePath + '/menu-category.html',
        link: function(scope) {

        	scope.itens = [
        				 {title: 'Alimentos e bebidas', icon: 'alimentos_bebidas.png'},
        				 {title: 'Bancos e Cartões', icon: 'bancos_cartoes.png'},
        				 {title: 'Bebês e Criança', icon: 'bebe_crianca.png'},
        				 {title: 'Beleza e Estética', icon: 'beleza_estetica.png'},
        				 {title: 'Casa e Construção', icon: 'casa_construcao.png'},
        				 {title: 'E-commerce', icon: 'ecommerce.png'},
        				 {title: 'Educação', icon: 'educacao.png'},
        				 {title: 'Moda', icon: 'moda.png'},
        				 {title: 'Movéis e Decoração', icon: 'moveis_decoracao.png'},
        				 {title: 'Saúde', icon: 'saude.png'},
        				 {title: 'Telefonia, TV e Internet', icon: 'telefonia_internet.png'},
        				 {title: 'Turismo e Lazer', icon: 'turismo_lazer.png'},
        				 {title: 'Veículos e Acessórios', icon: 'veiculo_acessorio.png'},
        				 ];

        }
    };
});