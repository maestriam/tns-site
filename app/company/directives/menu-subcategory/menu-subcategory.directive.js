app.directive('menuSubcategory', function () {

    var basePath = 'app/company/directives/menu-subcategory';

    return {
        templateUrl: basePath + '/menu-subcategory.html',
        link: function(scope) {

            scope.itens = [
                            {'title': 'Acessórios'},
                            {'title': 'Bermudas'},
                            {'title': 'Bonés'},
                            {'title': 'Blusas'},
                            {'title': 'Bolsa e Mochilas'},
                            {'title': 'Calças'},
                            {'title': 'Calçados'},
                            {'title': 'Camisetas'},
                            {'title': 'Camisas'},
                            {'title': 'Casacos e Jaquetas'},
                            {'title': 'Chapéus'},
                            {'title': 'Macacões'},
                            {'title': 'Moda esportiva'},
                            {'title': 'Moda Íntima'},
                            {'title': 'Óculos'},
                          ];

        }
    };
});