app.directive('cardCompany', ['$location', 'Favorite', 
                    function ($location, Favorite) {

	var basePath = 'app/company/directives/card-company';

    return {
        templateUrl: basePath + '/card-company.html',
        scope: {
        	id: '@',
        	taps: '@',
            desc: '@',
            name: '@',
            rating: '@',
            category: '@',
        },
        link: function(scope) {

            scope.goSupport = (id) => {
                $location.path('/support/'+id);
            }

            scope.addFav = (customerId) => {
                Favorite.add(customerId, 
                (response) => {
                    alert('Favorito adicionado com sucesso');
                },
                (reason) => {

                });
                return false;
            }

        }
    };
}]);