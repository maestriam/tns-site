var DOMAIN = 'http://preview.maestriam.com.br/tns/';

app.config(['$mdIconProvider', function($mdIconProvider) {

}])

// Configurações de request
.config(['$httpProvider', function($httpProvider){
  
  $httpProvider.defaults.headers.common = {};
  $httpProvider.defaults.headers.post = {};
  $httpProvider.defaults.headers.put = {};
  $httpProvider.defaults.headers.patch = {};
  $httpProvider.defaults.headers.common["Accept"] = "application/json";
  $httpProvider.defaults.headers.common["Content-Type"] = "application/json";

}])

.config(['$mdThemingProvider', function($mdThemingProvider){

    $mdThemingProvider.theme('forest')
                      .primaryPalette('brown')
                      .accentPalette('green');


}])

.constant('TYPE_BASE', '2')
.constant('URL_BASE',  'http://138.197.81.59/api');

