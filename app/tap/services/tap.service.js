app.factory('Tap', ['$rootScope', '$http', 'URL_BASE', 'User', 
		   function($rootScope, $http, URL_BASE,  User) {

    /**
     * Lista todos os taps do usuário
     * 
     * @param  function success 
     * @param  function error   
     * @return void
     */
    function all(success, error)
    {
        var token = User.token();

        var callback = (response) => {

            if (response.data == null) {
                return error(response);
            }

            success(response.data);
        }

        $http({
            url: URL_BASE + '/taps?token=' + token,
            dataType: 'json',
            method: 'GET',
            timeout: 15000
        })
        .then(callback, error);
    }


    /**
     * Função para excluir um tap do usuário
     * 
     * @param  integer  tapId      
     * @param  integer  userId     
     * @param  integer  customerId 
     * @param  function success    
     * @param  function error      
     * @return void
     */
    function remove(tapId, customerId, success, error)
    {
        var user   = User.local();

        var params = tapId +'/'+ user.id +'/'+ customerId;

        var callback = (response) => {

            if (response.data == null) {
                return error(response);
            }

            if (response.data.message != 'Excluído.') {
                return error(response);
            }

            success(response.data);
        }

        $http({
            url: URL_BASE + '/taps/delete/' + params,
            dataType: 'json',
            method: 'DELETE',
            timeout: 15000
        })
        .then(callback, error);
    }
    
    return {
    	all: all,
        remove: remove
    }	
}]);