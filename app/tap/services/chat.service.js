app.factory('Chat', ['$http', 'URL_BASE', 'User', 
   function($http, URL_BASE,  User) {
   	
   	/**
   	 * Função responsável para começar uma nova chamada ao atendente
   	 * 
   	 * @param  function success 
   	 * @param  function error   
   	 * @return void
   	 */
   	var start = (customerID, success, error) => {

   		let token = User.token();

        let input = {
            token: token,
            customer: customerID
        }

        let callback = (response) => {

            console.log(response);

            if (response.data == null) {
                return error(response);
            }

            return success(response.data);
        }

        $http({
            url: URL_BASE + '/queue/start',
            dataType: 'json',
            method: 'POST',
            data: input,
            timeout: 15000
        })
        .then(callback, error);
   	}

   	/**
   	 * Função para envio de mensagens para o atendente
   	 * 
   	 * @param  integer  chatId  		
   	 * @param  string   message   
   	 * @param  function success 
   	 * @param  function error   
   	 * @return void
   	 */
   	var send = (chatId, message, success, error) => {

   		let token = User.token();

        let callback = (response) => {

            if (response.data == null) {
                return error(response);
            }

            return success(response.data);
        }

        let input = {
        	token: token,
        	chat: chatId,
        	message: message
        }

        $http({
            url: URL_BASE + '/message/send',
            dataType: 'json',
            data: input,
            method: 'POST',
            timeout: 15000
        })
        .then(callback, error);
   	}

   	/**
   	 * Função para receber as mensagens do atendente
   	 * 
   	 * @param  integer  chatId  
   	 * @param  function success 
   	 * @param  function error  
   	 * @return void
   	 */
   	var get = (chatId, success, error) => {

   		  let token  = User.token();
        let params = '/message/'+chatId+'/get?token='+token;

        let callback = (response) => {

            if (response.data == null) {
                return error(response);
            }

            return success(response.data);
        }

        $http({
            url: URL_BASE + params,
            dataType: 'json',
            method: 'GET',
            timeout: 15000
        })
        .then(callback, error);
   	}


   	return {
   		start: start,
   		send: send,
   		get: get,
   	}
}]);