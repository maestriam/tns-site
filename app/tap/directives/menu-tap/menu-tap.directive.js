app.directive('menuTap', function () {

    var basePath = 'app/tap/directives/menu-tap';

    return {
        templateUrl: basePath + '/menu-tap.html',
        link: function(scope, elem, attr) {
            var vm = scope;
            
            vm.data = {};
            vm.data.cb1 = true;

            vm.todos = [
                        {title: 'Na fila', done: false, css: 'opened'},
                        {title: 'Aberto', css: 'sended'},
                        ];
        }
    };
});