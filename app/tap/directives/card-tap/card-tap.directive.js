app.directive('cardTap', ['$location', '$window', 'Tap', 
                function($location, $window, Tap) {

	var basePath = 'app/tap/directives/card-tap';

    return {
    	//restrict: 'E',
    	scope: {
    		show: '@',
            id: '@',
            taps: '@',
            name: '@',
            category: '@',
            desc: '@',
            status: '@',
            customerId: '@'
    	},
        templateUrl: basePath + '/card-tap.html',
        link: function(scope, elem, attr) {
        
            function goChat(id) {
                $location.path('/chat/'+id);
            }

            function getStatus()
            {
                if (scope.status == 'Na Fila') {
                    scope.stlStatus = 'queue';
                    scope.goChat    = () => {}
                }

                else if (scope.status == 'Em atendimento') {
                    scope.stlStatus = 'queue';
                    scope.goChat    = goChat;
                }
            }


            scope.delete = () => {

                Tap.remove(scope.id, scope.customerId,
                (response) => {
                    alert('Tap excluído com sucesso');
                    $window.location.reload();
                },
                (reason) => {
                    console.log(reason);
                })
            }

            getStatus();
        }
    };
}]);