app.controller('ChatController', function ($scope, $interval, $stateParams, Chat, User) {

	var vm = $scope;

	vm.status 	  = [];
	vm.talks   = [];

	vm.chatId     = $stateParams.companyId; 
	vm.newMessage = '';
	vm.user = User.local();


	vm.send = () => {

		if (vm.newMessage == '') {
			return false;
		}

		Chat.send(vm.chatId, vm.newMessage, 
		(response) => {
			vm.get();
			vm.newMessage = '';
		},
		(reason) => {
			console.log(reason);
		})
	}

	vm.get = () => {
		Chat.get(vm.chatId, 
		(response) => {
			vm.talks = response;
			console.log(response);
		},
		(reason)=>{

		})
	}

	vm.get();

	$interval(vm.get, 2000);

});