app.controller('TapController', function ($scope, $rootScope, $location, $mdDialog, Tap, User) {

	if (User.isLogged() == false) {
        return $location.path('/');
    }

	vm = $scope;

	Tap.all(
	(response) => {
		vm.taps = response;
	},
	(reason)=> {
		console.log(reason);
	});


	vm.status = [];
});