app.config(function($stateProvider, $urlRouterProvider){

	var index = {
		url: '/',
		templateUrl: 'app/base/views/home.html',
		controller: 'HomeController'
	}

	var profile = {
		url: '/profile',
		templateUrl: 'app/user/views/profile.html',
		controller: 'ProfileController'
	}

	var favorite = {
		url: '/favorite',
		templateUrl: 'app/favorite/views/favorite.html',
		controller: 'FavoriteController'
	}

	var taps = {
		url: '/taps',
		templateUrl: 'app/tap/views/taps.html',
		controller: 'TapController'	
	}

	var search = {
		url: '/search',
		templateUrl: 'app/company/views/search.html',
		controller: 'SearchController'	
	}

	var category = {
		url: '/search/category',
		templateUrl: 'app/company/views/category.html',
		controller: 'CategoryController'	
	}

	var support = {
		url: '/support/:companyId',
		templateUrl: 'app/company/views/support.html',
		controller: 'SupportController'	
	}

	var options = {
		url: '/options/:questionId',
		templateUrl: 'app/company/views/options.html',
		controller: 'OptionsController'	
	}

	var chat = {
		url: '/chat/:companyId',
		templateUrl: 'app/tap/views/chat.html',
		controller: 'ChatController'	
	}

	$stateProvider.state('index', index)
				  .state('profile', profile)
				  .state('search', search)
				  .state('taps', taps)
				  .state('category', category)
				  .state('favorite', favorite)
				  .state('support', support)
				  .state('options', options)
				  .state('chat', chat);

	
	$urlRouterProvider.otherwise('/');
});