app.factory('UserDialog', ['User', '$mdDialog', '$location',   
   function(User, $mdDialog, $location) {

	var path = 'app/user/views';

    var dialog = (message) => 
    {
        $mdDialog.show(
            $mdDialog.alert()
                     .title('TNS')
                     .textContent(message)
                     .ariaLabel('Alert Dialog Demo')
                     .ok('OK')
        );
    }

	/**
	 * Chama o modal de login
	 * 
	 * @param  function success 
	 * @param  function error   
	 * @return void
	 */
	var login = () =>
	{
		if (User.isLogged() == true) 
		{
			return false;	
		}

        $mdDialog.show({
            templateUrl: path + '/login.html',
            clickOutsideToClose:true,
            controller: 'LoginController',
        })
        .then(
        (response) => {
            //success(response);
        }, 
        (reason) => {
            //success(reason);            
        });
	}

	/**
	 * Chama o modal de boas-vindas ao usuário
	 * 
	 * @param  function success 
	 * @param  function error   
	 * @return void
	 */
	var welcome = () =>
	{
		$mdDialog.show({
            templateUrl: path + '/welcome.html',
            clickOutsideToClose:true,
            fullscreen: vm.customFullscreen,
            controller: 'WelcomeController',
        })
        .then(
        (response) => {
        	$location.path('/');
            //success(response);
        }, 
        (reason) => {
            //success(reason);            
        });
	}


    /**
     * Chama 
     * @return {[type]} [description]
     */
	var forgotPassword = () => 
	{
		$mdDialog.show({
            templateUrl: path + '/forgot.html',
            clickOutsideToClose:true,
            fullscreen: vm.customFullscreen,
            controller: 'ForgotController',
        })
        .then(
        (response) => {

        }, 
        (reason) => {
            //success(reason);            
        });
	}

	return {
		login: login,
		welcome: welcome,
        dialog: dialog,
		forgotPwd: forgotPassword
	}
}]);