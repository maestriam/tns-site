app.factory('User', ['$rootScope', '$http', 'LocalStorage', 'URL_BASE', 'TYPE_BASE', 
            function($rootScope, $http, LocalStorage, URL_BASE, TYPE_BASE) {

    /**
     * Salva os dados do usuário no local storage do usuário
     * 
     * @param Object user 
     * @return void
     */
    var setLocal = function(user)
    {
        LocalStorage.setObject('user', user);
    }

    /**
     * Retorna os dados do usuário no local storage do usuário
     * 
     * @return Object
     */
    var getLocal = function()
    {
        return LocalStorage.getObject('user');
    }

    /**
     * Retorna o token para executar funções básicas do usuário
     * 
     * @return string
     */
    var getToken = function()
    {
        var user =  LocalStorage.getObject('user');

        return user.token;
    }

    /**
     * Função para execução de login do usuário na API
     * 
     * @param  string   email    
     * @param  string   password 
     * @param  function success  
     * @param  function error    
     * @return void
     */
    var login = function(email, password, success, error)
    {
        var callback = (response) => {

            if (response.data.token == null) {
                return error(response.data);
            }

            var user = response.data.user;
            setLocal(user);
            success(response);
        }

        var input = {
            'email'   : email,
            'password': password,
            'type'    : TYPE_BASE
        }

        $http({
            url: URL_BASE + '/login',
            dataType: 'json',
            method: 'POST',
            timeout: 15000,
            data: input,
        })
        .then(callback, error);
    }


    /**
     * Função para cadastro de um novo usuário no sistema
     * 
     * @param  string  token    
     * @param  string  name     
     * @param  string  email    
     * @param  string  password 
     * @return void          
     */
    var signup = function(name, email, alias, password, success, error)
    {
        var input = {
            'name': name,
            'email': email,
            'alias': alias,
            'password': password
        }

        var callback = (response) => {

            if (response.data.user == null) {
                return error(response.data);
            }

            var user = response.data.user;
            setLocal(user);
            success(response);
        }

        $http({
            url: URL_BASE + '/register',
            dataType: 'json',
            method: 'POST',
            data: input,
            timeout: 15000
        })
        .then(callback, error);
    }


    /**
     * Exibir informações do usuário
     * 
     * @param  integer userId 
     * @return void
     */
    var get = function(success, error) 
    {
        var token = getToken();
        var input = {token: token}
        var url   = URL_BASE + '/info';

        var callback = (response) => 
        {
            if (! response.data) {
                return error(response);
            }

            return success(response.data);
        }

        $http({
            url: url,
            dataType: 'json',
            method: 'POST',
            data: input,
            timeout: 15000
        })
        .then(callback, error);
    }

    /**
     * Atualiza os dados do usuário na API 
     * 
     * @param  string   name     
     * @param  string   email    
     * @param  string   phone    
     * @param  string   password 
     * @param  function success  
     * @param  function error    
     * @return void
     */
    var update = function(name, email, phone, password, success, error)
    {
        var token = getToken(); 

        var callback = (response) => {

            if (response.data.user == null) {
                return error(response.data);
            }

            var user = response.data.user;
            setLocal(user);
            return success(response);
        }

        var input = {
            'type' : TYPE_BASE,
            'name' : name,
            'token': token,
            'phone': phone,
            'email': email,
            'password': password
        }

        $http({
            url: URL_BASE + '/updateall',
            dataType: 'json',
            method: 'POST',
            data: input,
            timeout: 15000
        })
        .then(callback, error);
    }

    /**
     * Desloga o usuário do sistema
     * 
     * @return void
     */
    var logout = function()
    {
        LocalStorage.remove('user');
    }

    /**
     * Verifica se o usuário está logado
     * 
     * @return void
     */
    var isLogged = function() 
    {
        var user = getLocal();

        if (user.id != undefined) {
            return true;
        }

        return false;
    }

     /**
     * Função para resgatar a chave de acesso para troca de senha
     * @return void
     */
    var requestChange = (email, success, error) => 
    {
        var callback = (response) => {
            if (response.data.message != 'User request created with success') {
                return error(response.data);
            }
            success(response);
        }

        var input = {
            'email'   : email
        }

        $http({
            url: URL_BASE + '/request_change_password',
            dataType: 'json',
            method: 'POST',
            timeout: 15000,
            data: input,
        })
        .then(callback, error);
    }

    return {
        get: get,
        login: login,
        logout: logout,
        update: update,
        signup: signup,
        local: getLocal,
        token: getToken,
        isLogged: isLogged,
        requestPwd: requestChange
    }
}]);