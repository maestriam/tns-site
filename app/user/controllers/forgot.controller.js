app.controller('ForgotController', function ($scope, $window, $mdDialog, User, UserDialog) {

	var vm = $scope;
	vm.user = {}

	vm.request = () => 
	{

		User.requestPwd(vm.user.email,
		(response) => {
			UserDialog.dialog('E-mail enviado para recuperação de senha');
		},
		(reason) => {
			UserDialog.dialog('Falha na recuperação de senha');
		})
	}

});