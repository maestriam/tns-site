app.controller('WelcomeController', function ($scope, $mdDialog) {

	var vm = $scope;

	var msgs = [];
	msgs.push("Já conhece o \nTAP N'SOLVE?");
	msgs.push("A solução em atendimento\ne assistência das maiores empresas\ndo Brasil na palma de sua mão");
	msgs.push("Abra e acompanhe seus taps\npelo app e as empresas entram\nem contato com você.\n\nSem perder tempo no telefone\nou em chats na internet!");

	vm.msg     = msgs[0];
	vm.btlabel = 'Pular';
	vm.step    = 0;
	vm.currentNavItem = "step-0";

	vm.step = function(i) {
		console.log(i);
		vm.msg  = msgs[i];
		//vm.step = i;
		vm.btlabel = (i == 2) ? 'Começar!' : 'Pular';
	}

	vm.close = function() {
		$mdDialog.hide();
	}
});