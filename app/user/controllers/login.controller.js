app.controller('LoginController', function ($scope, $window, $mdDialog, User, UserDialog) {

    var vm = $scope;

    vm.loading = false;

    // Cadastra um novo usuário
    vm.signup = () => 
    {
        var user = vm.new
        vm.loading = true;
        
        User.signup(user.name, user.email, user.alias, user.password,
        (response) => {
            vm.loading = false;
            UserDialog.welcome();
        },
        (reason) => {
            vm.loading = false;
            return alert(reason.message);
        });
    }

    // Faz o login do usuário no sistema
    vm.signin = () => 
    {
        var user = vm.user
        vm.loading = true;
        
        User.login(user.email, user.password, 
        (response) => {
            vm.loading = false;
            $window.location.reload();
        },
        (reason) => {
            vm.loading = false;
            return alert(reason.message);
        });
    }

    // 
    vm.forgot = () => 
    {
        UserDialog.forgotPwd();
    }
});