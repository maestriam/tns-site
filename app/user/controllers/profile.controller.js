app.controller('ProfileController', function ($scope, $rootScope, $location, $rootScope, $mdDialog, User) {

    var vm = $scope;
	
    if (User.isLogged() == false) {
        $location.path('/');
    }

    function getUser()
    {
        User.get(
        (response) => {
            vm.user = response;
        },
        (reason) => {

        });
    }

    function dialog()
    {
        var confirm = $mdDialog.alert()
                               .title('Usuário atualizado')
                               .textContent('Dados salvos com sucesso')
                               .ok('Sim');

        $mdDialog.show(confirm);
    }


    vm.save = () => {

        var name     = vm.user.name;
        var email    = vm.user.email;
        var phone    = vm.user.phone;
        var password = vm.user.password;

        User.update(name, email, phone, password,
        (response) => {
            console.log(response);
            $rootScope.$broadcast('user-updated');
    		dialog();
    	},
    	(reason) => {
    		//console.log(reason);
    	});
    }

    getUser();

});