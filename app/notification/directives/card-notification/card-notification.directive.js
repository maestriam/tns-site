app.directive('cardNotification', function () {

	var basePath = 'app/notification/directives/card-notification';

    return {
        templateUrl: basePath + '/card-notification.html'
    };
});