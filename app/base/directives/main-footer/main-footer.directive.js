app.directive('mainFooter', ['$window', function ($window) {

	var basePath = 'app/base/directives/main-footer';

    return {
        templateUrl: basePath + '/main-footer.html',
        
        link: function(scope, elem) {


        	elem.on('scroll', (event) => {

        		console.log(event);

        	});
        }
    };
}]);