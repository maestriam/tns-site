app.directive('navHeader', ['$location', '$rootScope', 'Company', 
                  function ($location, $rootScope, Company) {

	var basePath = 'app/base/directives/nav-header';

    return {
        templateUrl: basePath + '/nav-header.html',
        link: function(scope)
        {        	
            scope.query    = null;
            scope.category = null;

            //
            Company.categories(
            (response) => {
                scope.categories = response;
            },
            (reason) => {
                console.log(reason);
            });

            //
		    scope.openMenu = function($mdMenu, ev) {
      			originatorEv = ev;
      			$mdMenu.open(ev);
    		};

            //
    		scope.goTaps = function() {
    			$location.path('/taps');
    		}

            //
            scope.goFavorites = function() {
                $location.path('/favorite');
            }


            scope.search = function()
            {
                if (scope.query == '') {
                    $rootScope.$broadcast('search.company', null);
                }

                Company.search(scope.query, scope.category,
                (response) => {
                    $rootScope.$broadcast('search.company', response);
                },
                (reason) => {

                });
            }
        }
    };
}]);