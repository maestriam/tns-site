app.directive('menuHistory', function () {

	var basePath = 'app/base/directives/menu-history';

    return {
        templateUrl: basePath + '/menu-history.html'
    };
});