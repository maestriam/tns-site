app.directive('menuProfile', ['$location', '$window', '$rootScope', 'User', 
					function ($location, $window, $rootScope, User) {

	var basePath = 'app/base/directives/menu-profile';

    return {
        templateUrl: basePath + '/menu-profile.html',
        link: function(scope) {

        	scope.menu = [
        		{label: 'Meu Perfil',    url: '/profile'},
        		{label: 'Notificações',  url: '/notification'},
        		{label: 'Configurações', url: '/configs'},
        		{label: 'Sobre o TNS',   url: '/about'},
        		{label: 'Fale conosco',  url: '/contact'},
        	]

            scope.user = User.local();

            $rootScope.$on('user-updated', function(event, user){
                scope.user = User.local();
            });

        	scope.goTo = (url) => {
        		$location.path(url);
        	} 

        	scope.logout =() => {
        		User.logout();
                $window.location.reload();
        	}
        }
    };
}]);