app.controller('HomeController', function ($scope, $rootScope, $mdDialog, UserDialog, Company) {

    vm = $scope;
    vm.companies = null;
    vm.showSearch = false; 

    // 
    Company.categories(
    (response) => {
        vm.categories = response;

        let name = vm.categories[0].name;
        let companies = vm.categories[0].companies;

        vm.goto(name, companies);
    }, 
    (reason) => {

    });

    // Lista todas as empresas 
    Company.all(
    (response) => {
        vm.newCompanies = response.data;
    },
    (reason) => {
        console.log(reason);
    });

    $rootScope.$on('search.company', (events, args) => {
        
        if (args == null) {
            vm.showSearch = false;
            return false;
        }

        vm.search = args;
        vm.showSearch = true;
    });


    //
    vm.goto = (name, companies) => {
        vm.category  = name;
        vm.companies = companies;
    }

    vm.getCat = (id) => {
        return Company.categoryById(id);
    }

    UserDialog.login();
});