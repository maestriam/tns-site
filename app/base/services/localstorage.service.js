app.factory('LocalStorage', ['$window', function($window) {
  return {
    set: function(key, value)
    {
      $window.localStorage[key] = value;
    },
    get: function(key, defaultValue)
    {
      return $window.localStorage[key] || defaultValue;
    },
    setObject: function(key, value)
    {
      $window.localStorage[key] = JSON.stringify(value);
    },
    getObject: function(key)
    {
      var obj = $window.localStorage[key];

      if (obj == undefined) {
        return {};
      }

      return JSON.parse($window.localStorage[key]);
    },
    clearAll: function()
    {
      $window.localStorage.clear();
    },
    remove: function(key)
    {
      $window.localStorage.removeItem(key);
    }
  }
}]);