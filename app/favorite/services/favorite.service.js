app.factory('Favorite',['$http', 'URL_BASE', 'TYPE_BASE', 'User', 'LocalStorage',
			  function($http, URL_BASE, TYPE_BASE, User, LocalStorage) {

	/**
	 * Retorna todos os favoritos do usuário 
	 * 
	 * @param  function success 
	 * @param  function error   
	 * @return void
	 */
	function all(success, error)
	{
		var input = {
			token: User.token()
		}

		var callback = (response) => {
			let favorites = response.data.data;
			success(favorites);
		}

		$http({
            url: URL_BASE + '/favorites',
            dataType: 'json',
            method: 'POST',
            timeout: 15000,
            data:input
        })
        .then(callback, error);
	}


	/**
	 * Funcão para remover a marcação de favorito do usuário
	 * 
	 * @param  integer id       
	 * @param  function success 
	 * @param  function error   
	 * @return void
	 */
	var remove = (companyId, success, error) =>
	{
		var user   = User.local();
		var params = user.id + '/' + companyId;

		var callback = (response) => {
			//console.log(response);

			if (response.data == undefined) {
				return erro(response);
			}

			return success(response);
		}

		$http({
            url: URL_BASE + '/favorites/delete/' + params,
            dataType: 'json',
            method: 'DELETE',
            timeout: 15000,
        })
        .then(callback, error);
	}

	var add = (customerId, success, error) =>
	{
		var input = {
			token: User.token(),
			customer_id: customerId
		}

		var callback = (response) => {

			if (response.data == undefined) {
				return erro(response);
			}

			return success(response);
		}

		$http({
            url: URL_BASE + '/favorites/add',
            dataType: 'json',
            method: 'POST',
            timeout: 15000,
            data:input
        })
        .then(callback, error);
	}

	return {
		all: all,
		add: add,
		remove: remove
	}

}]);

