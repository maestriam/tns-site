app.directive('cardFavorite', ['$mdDialog', '$window', 'Favorite', 
                     function ($mdDialog, $window, Favorite) {

	var basePath = 'app/favorite/directives/card-favorite';

    return {
        templateUrl: basePath + '/card-favorite.html',
        scope: {
        	id: '@',
        	taps: '@',
            desc: '@',
            name: '@',
            rating: '@',
            category: '@',
        },
        link: function(scope) {

            scope.remove = (id) => {


                var confirm = $mdDialog.confirm()
                                       .title('Remover favorito')
                                       .textContent('Tem certeza que deseja fazer isso?')
                                       .ok('Sim')
                                       .cancel('Não');

                $mdDialog.show(confirm).then(
                () => {
                    Favorite.remove(id,
                    (response) => {
                        $window.location.reload();
                    },
                    (reason) => {

                    });
                }, 
                () => {
                });
            }

        }
    };
}]);