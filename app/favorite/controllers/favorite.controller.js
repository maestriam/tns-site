app.controller('FavoriteController', function ($scope, $rootScope, $location, Favorite, User, Company) {

	var vm = $scope;

	if (User.isLogged() == false) {
        return $location.path('/');
    }

	Favorite.all(
	(response) => {
		vm.favorites = response;

	},
	(reason)=> {
		console.log(reason);
	});

	vm.getCat = (id) => {
        return Company.categoryById(id);
    }


	vm.status = [];
});