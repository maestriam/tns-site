'use strict';

// Dependências
var gulp    = require('gulp');
var sass    = require('gulp-sass');
var concat  = require('gulp-concat');
var clean   = require('del');
var gsync   = require('gulp-sync')(gulp);
var connect = require('gulp-connect');
var open    = require('gulp-open');
var notify  = require("gulp-notify");

// Seus arquivos JS
var js  = [
    './assets/js/vendor.js',
    './app/app.module.js',
    './app/app.routes.js',
    './app/app.config.js',
    './app/base/**/*.js',
    './app/user/**/*.js',
    './app/favorite/**/*.js',
    './app/company/**/*.js',
    './app/company/directives/**/*.js',
    './app/company/filters/**/*.js',
    './app/notification/**/*.js',
    './app/notification/directives/**/*.js',
    './app/tap/**/*.js',
    './app/tap/directives/**/*.js',
];

// Arquivos JS do vendor
var vendorJs = [
    './node_modules/angular/angular.js', 
    './node_modules/angular-ui-router/release/angular-ui-router.min.js', 
    './node_modules/angular/angular.js',
    './node_modules/angular-aria/angular-aria.js',
    './node_modules/angular-animate/angular-animate.js',
    './node_modules/angular-material/angular-material.js',
    './node_modules/angular-jk-carousel/dist/jk-carousel.min.js',
    './node_modules/ng-infinite-scroll/build/ng-infinite-scroll.js'
]

// Arquivos CSS do projeto Angular
var css = [
    './assets/css/vendor.css',
    './assets/css/index.css'
];

// Arquivos CSS do vendor
var vendorCss = [
    './node_modules/angular-material/angular-material.css',
    './node_modules/font-awesome/css/font-awesome.min.css',
    './node_modules/angular-jk-carousel/dist/jk-carousel.min.css',
];

// Tarefa de minificação do Javascript dos vendors
gulp.task('vendor', function () {
    return gulp.src(vendorJs)                        
    .pipe(concat('vendor.js')) 
    .pipe(gulp.dest('./assets/js/'))
});

// Tarefa de minificação do Javascript
gulp.task('js', function () {
    return gulp.src(js)                        
    .pipe(concat('script.min.js')) 
    .pipe(gulp.dest('./assets/dist/'))
    .pipe(connect.reload());
});

// Tarefa de minificação do CSS
gulp.task('css', function () {
    return gulp.src(css)                        
    .pipe(concat('style.min.css')) 
    .pipe(gulp.dest('./assets/dist/'))
    .pipe(connect.reload());
});

// Tarefa de minificação do CSS dos vendors
gulp.task('css-vendor', function () {
    return gulp.src(vendorCss)                        
    .pipe(concat('vendor.css')) 
    .pipe(gulp.dest('./assets/css/'))
});

// Tarefa de conversão de SASS para CSS
gulp.task('sass', function () {
    var path = ['./scss/*.scss'];
    return gulp.src(path)
        .pipe(sass().on('error', function(err) {
            return notify().write(err);
        }))
        .pipe(gulp.dest('./assets/css/'));
});

// Salvar as fontes 
gulp.task('fonts', function(){
    var path = ['node_modules/font-awesome/fonts/fontawesome-webfont.*'];
    return gulp.src(path)
               .pipe(gulp.dest('./assets/fonts/'));
})

// Limpar arquivo JS
gulp.task('clean-js', function () {
    return clean(['assets/dist/script.min.js'], {read: false, force: true});
});


// Limpar arquivo CSS
gulp.task('clean-css', function () {
    return clean(['assets/dist/style.min.css'], {read: false, force: true});
});

// Abrir navegador 
gulp.task('connect', function() {
    return connect.server({livereload: true});
});

gulp.task('open', function(){
    return gulp.src(__filename)
               .pipe(open({uri: 'http://localhost:8080'}));
})



var start     = ['connect', 'vendor','js','sass','css-vendor','css','fonts', 'open'];
var reloadJs  = ['clean-js','js'];
var relaodCSS = ['clean-css','sass','css'];

// Início
gulp.task('default', gsync.sync(start));
    
// Vigia os arquivos SASS
gulp.watch('./scss/**/*',  gsync.sync(relaodCSS));
gulp.watch('./scss/**/**/*', gsync.sync(relaodCSS));

// Vigia os arquivos Js
gulp.watch('./app/**/**/*', gsync.sync(reloadJs));
