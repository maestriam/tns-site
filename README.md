## Bem-vindo ao Site Tap N Solve

Este projeto é SPA(Single Application Page) para o projeto Tap 'N Solve.

## Instalação 

``git clone https://bitbucket.org/maestriam/tns-site.git``

``npm install``

``gulp`` 

## Principais ferramentas utilizadas

* AngularJS 1.6.+
* Angular Material
* NPM 
* Gulp
* SASS


## Estrutura e convenções do projeto

* A estrutura deste projeto foi baseado em módulos, onde cada módulo possui uma funcionalidade específica e detém todas as regras de negócio de sua especialidade.

* Um módulo é divido em alto nível por funcionalidade e baixo nível por tipos de componentes.

* Os componentes que compõe um módulo são: 

```
module-name
	├── controllers	// Interface entre os serviços e o layout
	├── directives  // Diretivas reutílizaveis do módulo
    	├── filters     // Formatações e converões específicas do módulo
    	├── services    // Regras de negócios e integração com a API
    	└── views       // Arquivo HTML do layout do sistema
```

* Desta maneira, o projeto será semelhante ao exemplo abaixo:

```
app/
├── index.html
├── app.module.js
├── app.config.js
├── app.routes.js
└── user/
    ├── controllers
    │		├── profile.controller.js
    │		└── favorite.controller.js
    ├── directives
    │ 		├── directive1
    │		│	├── directive1.directive.js
    │		│	└── directive1.html
    │ 		└── directive2
    │			├── directive2.controller.js
    │			└── directive2.html
    ├── services
    │		├── srvc1.service.js
    │		├── srvc2.service.js
    │		├── srvc3.service.js
    │		└── srvc4.service,js
    ├── views
    │       ├── profile.html
    │       └── favorite.html
    └── filters
    		├── fltr1.filter.js
    		├── fltr2.filter.js
    		├── fltr3.filter.js
    		└── fltr4.filter,js
```


## Módulos do projeto

* Atualmente, o projeto conta com os seguintes módulos com as suas respectivas responsabilidades:

```
app/
├── base            // Responsável pelo layout, rotas genéricas (index, about, etc) e estruturas comuns no projeto
├── company         // Responsável pelas regras de negócio sobre a empresas cadastradas
├── favorite        // Responsável pelas regras de negócio sobre favoritos do usuário
├── notification    // Responsável pela parte habilitar/desabilitar notificações do sistema
├── tap             // Responsável pela parte de aberturas de chamados
└── user            // Responsável pela parte de informações básicas do usuários
```

## Específicações de editor

* Tabspace utilizado = 4
* Limite de máximo de coluna = 70
